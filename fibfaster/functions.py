#!/bin/env python3
from numpy import matrix
import math
from functools import lru_cache

class FibFaster:
    """A selection of Fibonacci number finder implementations."""

    @lru_cache(maxsize = 10000)
    def recurse(self, n):
        if n == 1:
            value = 1
        elif n == 2:
            value = 1
        elif n > 2:
            value = self.recurse(n-1) + self.recurse(n-2)
        return value

    @lru_cache(maxsize = 10000)
    def matrix(self, n):
        return (matrix(
        '0 1; 1 1' if n >= 0 else '-1 1; 1 0', object
        ) ** abs(n))[0, 1]

    @lru_cache(maxsize = 10000)
    def generator(self, n):
        def gen(n):
            a = 0
            b = 1
            for i in range(1, n+1):
                a, b = b, a + b
            yield a
        for answer in gen(n):
            pass
        return answer

    @lru_cache(maxsize = 10000)
    def gold(self, n):
        """Use with caution, no testing in place for this method. """
        square_root_5 = math.sqrt(5)
        golden_ratio = (square_root_5 + 1) / 2
        return int((golden_ratio ** n) / square_root_5 + 0.5)


