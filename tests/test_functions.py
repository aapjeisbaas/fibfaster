#!/bin/env python3
import unittest
from fibfaster import FibFaster
ff = FibFaster()

fiblist = [(1, 1), (2, 1), (3, 2), (4, 3), (5, 5), (6, 8), (7, 13), (8, 21), (9, 34), (10, 55), (11, 89), (12, 144),
           (130, 659034621587630041982498215), (169, 93202207781383214849429075266681969), (198, 107168651819712326877926895128666735145224)]

class TestFibonacciSequence(unittest.TestCase):
    def test_sequence(self):
        # Test normal operation with valid input
        for (i, n) in fiblist:
            self.assertEqual(ff.recurse(i), n)
            self.assertEqual(ff.matrix(i), n)
            self.assertEqual(ff.generator(i), n)

