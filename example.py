#!/bin/env python3
import timeit
from fibfaster import FibFaster
ff = FibFaster()
n = 1000000

start = timeit.default_timer()

answer = ff.matrix(n)

stop = timeit.default_timer()
duration = stop - start

print(str(answer)[:20] + "..." + str(answer)[-20:])
print('Got to the ' + str(n) +'th fibonacci number in: ' + str(duration) + ' secconds')

